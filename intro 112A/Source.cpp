#include<iostream>
#include<fstream>

using namespace std;

void ToLower(char*);
int Compare(char*, char*);

int main()
{
	char String1[100],String2[100];
	cin >> String1>>String2;	
	ToLower(String1);
	ToLower(String2);	
	cout << Compare(String1, String2);		
}
void ToLower(char* String)
{
	unsigned char i=0;
	while (String[i]!='\0')
	{
		String[i] = (char)tolower(String[i]);
		i++;
	}
}
int Compare(char* String1, char* String2)
{
	unsigned char i = 0;
	while (String1[i] != '\0')
	{
		if (String1[i] == String2[i])
		{
			i++;
			continue;
		}
		if (String1[i] < String2[i])return -1;
		else if (String1[i] > String2[i])return 1;
	}
	return 0;
}